import { Component } from '@angular/core';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { NavController, IonicPage, ToastController, FabContainer, PopoverController, Content, Platform } from 'ionic-angular';


@Component({
  selector: 'page-hello-ionic',
  templateUrl: 'hello-ionic.html'
})
export class HelloIonicPage {
  constructor(private iab: InAppBrowser,public navCtrl: NavController) {

  }

  test()
  {
    this.navCtrl.push("pruebas");
  }
  
  
}
