import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { ThemeableBrowser, ThemeableBrowserOptions, ThemeableBrowserObject } from '@ionic-native/themeable-browser';

/**
 * Generated class for the PruebasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
  name : "pruebas"
})
@Component({
  selector: 'page-pruebas',
  templateUrl: 'pruebas.html',
})
export class PruebasPage {

  browser: InAppBrowser;
  url:string ='http://ionicframework.com/docs/v2/native/inappbrowser/';

  constructor(public navCtrl: NavController, public navParams: NavParams,private iab: InAppBrowser,private themeableBrowser: ThemeableBrowser) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PruebasPage');
    //let options ='location=no,toolbar=no,hidden=no,zoom=no,closebuttoncaption=Done,hardwareback=no';
    //const browser = this.iab.create('https://docs.google.com/forms/d/e/1FAIpQLSf355PPgTh_iLeNnHM6jZ6Bi6f5bavslBtpxkMzqtHtD0YyKw/viewform','_blank',options);

    const options: ThemeableBrowserOptions = {
      toolbar: {
          height: 44,
          color: '#f0f0f0ff'
      },
      title: {
          color: '#003264ff',
          showPageTitle: true
      },
      closeButton: {
        wwwImage: 'assets/imgs/close.png',
        align: 'left',
        event: 'closePressed'
    }
 };
 
 const browser: ThemeableBrowserObject = this.themeableBrowser.create('https://docs.google.com/forms/d/e/1FAIpQLSf355PPgTh_iLeNnHM6jZ6Bi6f5bavslBtpxkMzqtHtD0YyKw/viewform', '_blank', options);
 //const browser: ThemeableBrowserObject = this.themeableBrowser.create('https://ionic.io', '_blank', options); 
}

}
